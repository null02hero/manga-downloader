const fetchLibrary = require('./fetch-library')
const fetchBook = require('./fetch-book')
const downloadChapter = require('./download-chapter')

module.exports = {
  fetchLibrary,
  fetchBook,
  downloadChapter,
}
