// import httpClient from "../../http/client"
// import cheerio from "cheerio"

const httpClient = require('../../http/client')
const cheerio = require('cheerio')
const _ = require('lodash')
const fs = require('fs')
const pLimit = require('p-limit');

const limit = pLimit(3);

const listHomePageURL = "https://www.komikgue.com/manga-list"

const fetchLibrary = async () => {

  let listFirstPageResult = await fetchListPage(1)

  let {
    totalPages,
  } = listFirstPageResult;

  let listPageResults = [
    listFirstPageResult,
    ...await Promise.all(_.range(2, totalPages).map(pageNumber => limit(() => fetchListPage(pageNumber)))),
  ]

  let libraries = listPageResults.reduce((prev, curr) => {
    return [
      ...prev,
      ...curr.books,
    ]
  }, [])

  let json = JSON.stringify(libraries, null, 2)


  prepareDir()

  fs.writeFileSync('./libraries/komikgue/list.json', json)

}

const prepareDir = () => {
  let downloadsDir = "./libraries"
  if (!fs.existsSync(downloadsDir)) {
    fs.mkdirSync(downloadsDir)
  }

  let komikgueDir = downloadsDir + "/komikgue"
  if (!fs.existsSync(komikgueDir)) {
    fs.mkdirSync(komikgueDir)
  }

  let bookDir = komikgueDir + "/book"
  if (!fs.existsSync(bookDir)) {
    fs.mkdirSync(bookDir)
  }

  return {
    downloadsDir,
    komikgueDir,
    bookDir,
  }
}


const fetchListPage = async (pageNumber) => {
  console.log(`[komikgue][fetch-library][${pageNumber}] start job`)
  let listPageHTML = await httpClient({ uri: listHomePageURL, qs: { page: pageNumber } })
  let listPageResult = parseListPageHTML(listPageHTML)
  console.log(`[komikgue][fetch-library][${pageNumber}] completed, listPageResult.books=${listPageResult.books.length}`)

  return listPageResult
}


const parseListPageHTML = (listPageHTML) => {
  const $ = cheerio.load(listPageHTML)

  let books = $(".content .media").toArray().map(parseBookElement($))

  let paginationListElement = $('.content ul.pagination li').toArray()
  let totalPages = parseInt($(paginationListElement[paginationListElement.length - 2]).find('a').text())
  let listPageResult = {
    books,
    totalPages,
  }

  return listPageResult
};

const parseBookElement = ($) => function (bookHTMLElement) {

  let url = $(bookHTMLElement).find("a.chart-title").attr('href')

  let book = {
    id: url.split('/').pop(),
    title: $(bookHTMLElement).find("a.chart-title > strong").text(),
    url,
    rating: parseFloat($(bookHTMLElement).find(".media-body > span").text()),
    status: $(bookHTMLElement).find('.media-body > div > dd > span.label').text(),
    tags: $(bookHTMLElement).find('.media-body > div > dd > a').toArray().map(el => $(el).text())
  }
  return book
}

module.exports = fetchLibrary
