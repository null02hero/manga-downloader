const httpClient = require('../../http/client')
const cheerio = require('cheerio')
const _ = require('lodash')
const fs = require('fs')
const pLimit = require('p-limit');
const moment = require('moment')
// require('moment/locale/id');


const limit = pLimit(10);

const fetchBook = async (book) => {

  console.log(`[komikgue][fetch-book][${book.id}] start job`)
  console.log(`[komikgue][fetch-book][${book.id}] title=${book.title} url=${book.url}`)
  let bookDetailHTML = await httpClient({ uri: book.url })

  let bookDetail = parseBookDetailHTML(book)(bookDetailHTML)

  let json = JSON.stringify(bookDetail, null, 2)


  fs.writeFileSync(`libraries/komikgue/book/${book.id}.json`, json)

  console.log(`[komikgue][fetch-book][${book.id}] completed, totalChapters=${bookDetail.chapters.length}`)

}

const parseBookDetailHTML = (book) => (bookDetailHTML) => {
  const $ = cheerio.load(bookDetailHTML)

  let bookDetail = {
    ...book,
    authors: $('dd[itemprop="author"] a').toArray().map(el => $(el).text()),
    cover_image: $(`#${book.id} > div:nth-child(5) > div.col-sm-4 > div > img`).attr('src'),
    year_release: $('dt:contains("Waktu rilis")').next('dd').text(),
    chapters: $('td.chapter').toArray().map(el => {
      return {
        chapter_number: $(el).find('a span').text(),
        url: $(el).find('a').attr('href'),
        release_date: moment($(el).next('td.date').text(), "DD MMM. YYYY").format("YYYY-MM-DD"),
      }
    })
  }

  return bookDetail
}

module.exports = fetchBook
