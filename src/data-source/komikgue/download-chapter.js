const httpClient = require('../../http/client')
const request = require('request')
const cheerio = require('cheerio')
const _ = require('lodash')
const fs = require('fs')
const path = require("path");

const pLimit = require('p-limit');
const limit = pLimit(3);

const downloadChapter = async (bookID, chapterNumber) => {
  const book = require(`../../../libraries/komikgue/book/${bookID}.json`)

  const bookChapter = _.find(book.chapters, ['chapter_number', chapterNumber])
  const bookChapterIndex = _.findIndex(book.chapters, ['chapter_number', chapterNumber])

  console.log(`[komikgue][download-manga-chapter][${bookID}][${chapterNumber}] downloading title=${book.title}`)

  const paths = prepareDir(bookID, chapterNumber)

  let chapterPageHTML = await httpClient({ uri: `${bookChapter.url}/1` })

  let imageLinks = parseChapterPageHTML(chapterPageHTML)


  let filenames = await Promise.all(imageLinks.map(imageLink => limit(() => downloadImage(bookID, chapterNumber)(paths, imageLink))))

  console.log(`[komikgue][download-manga-chapter][${bookID}][${chapterNumber}] complete`)
  console.log(`[komikgue][download-manga-chapter][${bookID}][${chapterNumber}] totalPages=${filenames.length}`)

  return filenames
}

const downloadImage = (bookID, chapterNumber) => (paths, imageLink) => new Promise((resolve, reject) => {
  let basename = path.basename(imageLink)
  let filename = `${paths.chapterDir}/${basename}`;
  if (fs.existsSync(filename)) {
    return resolve(filename)
  }
  console.log(`[komikgue][download-manga-chapter][${bookID}][${chapterNumber}][download-image] imageLink=${imageLink} target=${filename}`)

  request({ uri: imageLink, timeout: 50000 })
    .on('end', () => {
      console.log(`[komikgue][download-manga-chapter][${bookID}][${chapterNumber}][download-image] imageLink=${imageLink} complete`)
      resolve(filename)
    })
    .on('error', (error) => {
      console.log(`[komikgue][download-manga-chapter][${bookID}][${chapterNumber}][download-image] imageLink=${imageLink} error`, error)
      setTimeout(() => {
        fs.unlinkSync(filename)
        downloadImage(bookID, chapterNumber)(paths, imageLink).then(resolve)
      }, 1000)
    })
    .pipe(fs.createWriteStream(filename))

})

const parseChapterPageHTML = (chapterPageHTML) => {
  const $ = cheerio.load(chapterPageHTML)

  let imageLinks = $('#all > img').toArray().map(el => $(el).attr('src'))

  return imageLinks

}

const prepareDir = (bookID, chapterNumber) => {
  let downloadsDir = "./downloads"
  if (!fs.existsSync(downloadsDir)) {
    fs.mkdirSync(downloadsDir)
  }

  let komikgueDir = downloadsDir + "/komikgue"
  if (!fs.existsSync(komikgueDir)) {
    fs.mkdirSync(komikgueDir)
  }

  let bookDir = komikgueDir + "/" + bookID
  if (!fs.existsSync(bookDir)) {
    fs.mkdirSync(bookDir)
  }


  let chapterDir = bookDir + "/" + chapterNumber
  if (!fs.existsSync(chapterDir)) {
    fs.mkdirSync(chapterDir)
  }

  return {
    downloadsDir,
    komikgueDir,
    bookDir,
    chapterDir,
  }
}

module.exports = downloadChapter
