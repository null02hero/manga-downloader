const httpClient = require('../../http/client')
const cheerio = require('cheerio')
const _ = require('lodash')
const fs = require('fs')
const pLimit = require('p-limit');
const moment = require('moment')
require('moment/locale/id');

moment.updateLocale('id', {
  monthsShort: 'Jan_Feb_Mar_Apr_Mei_Jun_Jul_Agu_Sep_Okt_Nov_Des'.split('_'),
});


const limit = pLimit(10);

const fetchBook = async (book) => {

  console.log(`[pecintakomik][fetch-book][${book.id}] start job`)
  console.log(`[pecintakomik][fetch-book][${book.id}] title=${book.title} url=${book.url}`)
  let bookDetailHTML = await httpClient({ uri: book.url })

  let bookDetail = parseBookDetailHTML(book)(bookDetailHTML)

  let json = JSON.stringify(bookDetail, null, 2)


  fs.writeFileSync(`libraries/pecintakomik/book/${book.id}.json`, json)

  console.log(`[pecintakomik][fetch-book][${book.id}] completed, totalChapters=${bookDetail.chapters.length}`)

}

const parseBookDetailHTML = (book) => (bookDetailHTML) => {
  const $ = cheerio.load(bookDetailHTML)

  moment.locale('id');

  let bookDetail = {
    ...book,
    authors: $('th:contains("Penulis")').toArray().map(el => $(el).next().text().trim()),
    cover_image: $(`.imgprop meta[itemprop="url"]`).attr('content'),
    year_release: $('th:contains("Tahun Terbit")').toArray().map(el => $(el).next().text().trim()).pop(),
    chapters: $('.bxcl ul > li').toArray().map(el => {
      return {
        chapter_number: $(el).find('span.lchx > a').text().replace(/chapter /ig,''),
        url: $(el).find('span.lchx > a').attr('href'),
        release_date: moment($(el).find('span.dt').text(), "MMM DD, YYYY").format("YYYY-MM-DD"),
      }
    })
  }

  return bookDetail
}

module.exports = fetchBook
