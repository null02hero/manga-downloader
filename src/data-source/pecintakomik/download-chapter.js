const httpClient = require('../../http/client')
const request = require('request')
const cheerio = require('cheerio')
const _ = require('lodash')
const fs = require('fs')
const path = require("path");

const pLimit = require('p-limit');
const limit = pLimit(3);

const downloadChapter = async (bookID, chapterNumber) => {
  const book = require(`../../../libraries/pecintakomik/book/${bookID}.json`)

  const bookChapter = _.find(book.chapters, ['chapter_number', chapterNumber])
  const bookChapterIndex = _.findIndex(book.chapters, ['chapter_number', chapterNumber])

  console.log(`[pecintakomik][download-manga-chapter][${bookID}][${chapterNumber}] downloading title=${book.title}`)

  const paths = prepareDir(bookID, chapterNumber)

  let chapterPageHTML = await httpClient({ uri: `${bookChapter.url}` })

  let imageLinks = parseChapterPageHTML(chapterPageHTML)

  let filenames = await Promise.all(imageLinks.map((imageLink,page) => limit(() => downloadImage(bookID, chapterNumber, page)(paths, imageLink))))

  console.log(`[pecintakomik][download-manga-chapter][${bookID}][${chapterNumber}] complete`)
  console.log(`[pecintakomik][download-manga-chapter][${bookID}][${chapterNumber}] totalPages=${filenames.length}`)

  return filenames
}

const downloadImage = (bookID, chapterNumber, page) => (paths, imageLink) => new Promise((resolve, reject) => {
  let basename = path.basename(imageLink)
  let filename = `${paths.chapterDir}/${_.padStart((page+1+''),3,'0')}-${basename}`;
  if (fs.existsSync(filename)) {
    return resolve(filename)
  }
  console.log(`[pecintakomik][download-manga-chapter][${bookID}][${chapterNumber}][download-image] imageLink=${imageLink} target=${filename}`)

  request({ uri: imageLink, timeout: 50000 })
    .on('end', () => {
      console.log(`[pecintakomik][download-manga-chapter][${bookID}][${chapterNumber}][download-image] imageLink=${imageLink} complete`)
      resolve(filename)
    })
    .on('error', (error) => {
      console.log(`[pecintakomik][download-manga-chapter][${bookID}][${chapterNumber}][download-image] imageLink=${imageLink} error`, error)
      setTimeout(() => {
        fs.unlinkSync(filename)
        downloadImage(bookID, chapterNumber)(paths, imageLink).then(resolve)
      }, 1000)
    })
    .pipe(fs.createWriteStream(filename))

})

const parseChapterPageHTML = (chapterPageHTML) => {
  const $ = cheerio.load(chapterPageHTML)

  let imageLinks = $('#readerarea p > img').toArray().map(el => $(el).attr('src'))
  if(imageLinks.length === 0){
    imageLinks = $('#readerarea div.separator > a > img').toArray().map(el => $(el).attr('src'))
  }

  return imageLinks

}

const prepareDir = (bookID, chapterNumber) => {
  let downloadsDir = "./downloads"
  if (!fs.existsSync(downloadsDir)) {
    fs.mkdirSync(downloadsDir)
  }

  let pecintakomikDir = downloadsDir + "/pecintakomik"
  if (!fs.existsSync(pecintakomikDir)) {
    fs.mkdirSync(pecintakomikDir)
  }

  let bookDir = pecintakomikDir + "/" + bookID
  if (!fs.existsSync(bookDir)) {
    fs.mkdirSync(bookDir)
  }


  let chapterDir = bookDir + "/" + chapterNumber
  if (!fs.existsSync(chapterDir)) {
    fs.mkdirSync(chapterDir)
  }

  return {
    downloadsDir,
    pecintakomikDir,
    bookDir,
    chapterDir,
  }
}

module.exports = downloadChapter
