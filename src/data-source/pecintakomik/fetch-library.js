// import httpClient from "../../http/client"
// import cheerio from "cheerio"

const httpClient = require('../../http/client')
const cheerio = require('cheerio')
const _ = require('lodash')
const fs = require('fs')
const pLimit = require('p-limit');

const limit = pLimit(1);

const listHomePageURL = "https://www.pecintakomik.net/daftar-manga"

const fetchLibrary = async () => {

  let listFirstPageResult = await fetchListPage(1)

  let {
    totalPages,
  } = listFirstPageResult;

  console.log(`[pecintakomik][fetch-library] totalPages=${totalPages}`)

  let listPageResults = [
    listFirstPageResult,
    ...await Promise.all(_.range(2, totalPages).map(pageNumber => limit(() => fetchListPage(pageNumber)))),
  ]

  let libraries = listPageResults.reduce((prev, curr) => {
    return [
      ...prev,
      ...curr.books,
    ]
  }, [])

  let json = JSON.stringify(libraries, null, 2)


  prepareDir()

  fs.writeFileSync('./libraries/pecintakomik/list.json', json)

}

const prepareDir = () => {
  let downloadsDir = "./libraries"
  if (!fs.existsSync(downloadsDir)) {
    fs.mkdirSync(downloadsDir)
  }

  let pecintakomikDir = downloadsDir + "/pecintakomik"
  if (!fs.existsSync(pecintakomikDir)) {
    fs.mkdirSync(pecintakomikDir)
  }

  let bookDir = pecintakomikDir + "/book"
  if (!fs.existsSync(bookDir)) {
    fs.mkdirSync(bookDir)
  }

  return {
    downloadsDir,
    pecintakomikDir,
    bookDir,
  }
}


const fetchListPage = async (pageNumber) => {
  console.log(`[pecintakomik][fetch-library][${pageNumber}] start job`)
  let listPageHTML = await httpClient({ uri: `${listHomePageURL}/page/${pageNumber}` })
  let listPageResult = parseListPageHTML(listPageHTML)
  console.log(`[pecintakomik][fetch-library][${pageNumber}] completed, listPageResult.books=${listPageResult.books.length}`)

  return listPageResult
}


const parseListPageHTML = (listPageHTML) => {
  const $ = cheerio.load(listPageHTML)

  let books = $(".listupd .utao").toArray().map(parseBookElement($))

  let paginationListElement = $('div.pagination a.page-numbers').toArray()
  let totalPages = parseInt($(paginationListElement[paginationListElement.length - 2]).text())
  let listPageResult = {
    books,
    totalPages,
  }

  return listPageResult
};

const parseBookElement = ($) => function (bookHTMLElement) {

  let url = $(bookHTMLElement).find("a.series").attr('href')

  let book = {
    id: url.split('/').filter(s=>s.length>0).pop(),
    title: $(bookHTMLElement).find("a.series").attr('title'),
    url,
    rating: undefined,
    status: undefined,
    tags: $(bookHTMLElement).find('span.gee a[rel="tag"]').toArray().map(el => $(el).text())
  }
  return book
}

module.exports = fetchLibrary
