// import { fetchLibrary } from './data-source/komikgue'
const _ = require('lodash')
const fs = require('fs')

const pLimit = require('p-limit');

const limit = pLimit(1);
const limitM = pLimit(1);

const main = async () => {


  const dataSource = process.env.DATA_SOURCE

  const downloadChapter = require(`./data-source/${dataSource}`).downloadChapter
  const fetchLibrary = require(`./data-source/${dataSource}`).fetchLibrary
  const fetchBook = require(`./data-source/${dataSource}`).fetchBook


  if (process.env.UPDATE_LIBRARY) {
    console.log(`[${dataSource}][update-library] start job`)

    await fetchLibrary()
    console.log(`[${dataSource}][update-library] job completed`)
  }

  if (process.env.FETCH_BOOK) {
    console.log(`[${dataSource}][fetch-book] start job`)
    let libraryListFileName = `./libraries/${dataSource}/list.json`
    if (!fs.existsSync(libraryListFileName)) {
      console.log(`[${dataSource}][fetch-book] Library list not exist, please run UPDATE_LIBRARY first`)
      return
    }

    let books = loadJSON(`./libraries/${dataSource}/list.json`)

    if (process.env.BOOK_ID) {
      let bookIDs = process.env.BOOK_ID.split(',')
      if (bookIDs.length > 0) {
        books = books.filter(book => bookIDs.includes(book.id))
      }
    }

    await Promise.all(books.map(book => limit(() => fetchBook(book))))

    console.log(`[${dataSource}][fetch-book] job completed`)
  }

  if (process.env.LIST_BOOK) {
    console.log(`[${dataSource}][list-book] start job`)
    let libraryListFileName = `./libraries/${dataSource}/list.json`
    if (!fs.existsSync(libraryListFileName)) {
      console.log('Library list not exist, please run UPDATE_LIBRARY first')
      return
    }
    let books = loadJSON(libraryListFileName)

    books.forEach(book => {
      try {
        let bookDetail = loadJSON(`./libraries/${dataSource}/book/${book.id}.json`)
        console.log(`${book.id} | ${book.title} | ${bookDetail.chapters.length} Chapters`)
      } catch (error) {
        console.log(`${book.id} | ${book.title}`)

      }
    })
    console.log(`[${dataSource}][list-book] job completed`)
  }

  if (process.env.DOWNLOAD_ALL_BOOK) {
    console.log(`[${dataSource}][download-all-manga] start job`)
    let libraryListFileName = `./libraries/${dataSource}/list.json`
    if (!fs.existsSync(libraryListFileName)) {
      console.log('Library list not exist, please run UPDATE_LIBRARY first')
      return
    }

    let books = loadJSON(libraryListFileName)

    await Promise.all(books.map(book => limit(() => downloadManga(downloadChapter)(dataSource, book.id))))
    console.log(`[${dataSource}][download-all-manga] job completed`)
  }

  if (process.env.DOWNLOAD_BOOK) {
    let bookIDs = process.env.BOOK_ID.split(',')

    await Promise.all(bookIDs.map(bookID => limit(() => downloadManga(downloadChapter)(dataSource, bookID))))
  }

  if (process.env.LIST_BOOK_CHAPTER) {
    let bookID = process.env.BOOK_ID

    console.log(`[${dataSource}][list-chapter][${bookID}] job started`)
    let bookDetailFileName = `./libraries/${dataSource}/book/${bookID}.json`
    if (!fs.existsSync(bookDetailFileName)) {
      console.log(`[${dataSource}][list-chapter][${bookID}] ERROR Book not found, please run UPDATE_LIBRARY or FETCH_BOOK first`)
      return
    }

    let book = loadJSON(bookDetailFileName)

    book.chapters.reverse().forEach(chapter => console.log(`${chapter.chapter_number} | ${chapter.release_date}`))

    console.log(`[${dataSource}][list-chapter][${bookID}] completed`)

  }

  if (process.env.DOWNLOAD_BOOK_CHAPTER) {
    let bookID = process.env.BOOK_ID
    let chapterNumber = process.env.CHAPTER_NUMBER

    console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] start job`)

    await downloadChapter(bookID, chapterNumber)
    console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] completed`)

  }

  if (process.env.ZIP_BOOK) {
    let bookID = process.env.BOOK_ID
    zipBook(dataSource, bookID)

  }

  if (process.env.CREATE_MOBI) {
    let bookID = process.env.BOOK_ID
    await createMobi(dataSource, bookID)

  }
}

const downloadManga = (downloadChapter) => async (dataSource, bookID) => {
  console.log(`[${dataSource}][download-manga][${bookID}] job started`)
  let bookDetailFileName = `./libraries/${dataSource}/book/${bookID}.json`

  if (!fs.existsSync(bookDetailFileName)) {
    console.log(`[${dataSource}][download-manga][${bookID}] ERROR Book not found, please run UPDATE_LIBRARY or FETCH_BOOK first`)
    return
  }

  let book = loadJSON(bookDetailFileName)

  let totalChapters = book.chapters.length
  let completed = 0
  await Promise.all(book.chapters.reverse().map(chapter => limitM(async () => {
    let chapterNumber = chapter.chapter_number

    console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] start job [${completed + 1}/${totalChapters}]`)
    await downloadChapter(bookID, chapterNumber)
    ++completed
    console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] completed [${completed}/${totalChapters}]`)
    return
  })))

  console.log(`[${dataSource}][download-manga][${bookID}] completed`)
  return
}

const loadJSON = filename => {
  return JSON.parse(fs.readFileSync(filename).toString())
}

const zipBook = (dataSource, bookID) => {
  console.log(`[${dataSource}][zip-book][${bookID}] start job`)

  const downloadedBookDir = `./downloads/${dataSource}/${bookID}`
  let chaptersDir = fs.readdirSync(downloadedBookDir).filter(f => !['.DS_Store'].includes(f) && f.indexOf('html') < 0).sort((a, b) => parseFloat(a) - parseFloat(b))
  let chapterChunksDir = _.chunk(chaptersDir, 20)
  chapterChunksDir.forEach(chapterChunkDir => {
    let chapterRange = `${chapterChunkDir[0]}-${chapterChunkDir[chapterChunkDir.length - 1]}`;

    console.log(`[${dataSource}][zip-book][${bookID}][${chapterRange}] start job`)
    let images = []
    chapterChunkDir.forEach(chapterDir => {
      let chapterImageFiles = fs.readdirSync(`${downloadedBookDir}/${chapterDir}`).map(f => `./${chapterDir}/${f}`)
      images.push(...chapterImageFiles)
    })
    let html = `<html>
  <body>
    ${images.map((f, i) => `<img src="${f}" alt="${bookID} - Chapter ${f.split('/')[1]} - Page ${i + 1}" />`).join("\n    ")}
  </body>
</html>`

    var zip = new require('node-zip')();

    zip.file('index.html', html)
    images.forEach(f => {
      zip.file(f, fs.readFileSync(`${downloadedBookDir}/${f}`))
    })

    var data = zip.generate({ base64: false, compression: 'DEFLATE' });
    let bookChunkChaptersFileName = `${downloadedBookDir}/${bookID} - ${chapterRange}.zip`;
    fs.writeFileSync(bookChunkChaptersFileName, data, 'binary');

    console.log(`[${dataSource}][zip-book][${bookID}][${chapterRange}] completed, filename=${bookChunkChaptersFileName}`)

  })

  console.log(`[${dataSource}][zip-book][${bookID}] completed, total files=${chapterChunksDir.length}`)
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const createMobi = async (dataSource, bookID) => {
  console.log(`[${dataSource}][create-mobi][${bookID}] start job`)

  const downloadedBookDir = `./downloads/${dataSource}/${bookID}`

  let chaptersDir = fs.readdirSync(downloadedBookDir).filter(f => fs.statSync(`${downloadedBookDir}/${f}`).isDirectory() && !['.DS_Store'].includes(f) && f.indexOf('html') < 0).sort((a, b) => parseFloat(a) - parseFloat(b))
  let chapterChunksDir = _.chunk(chaptersDir, 20)
  await asyncForEach(chapterChunksDir, async (chapterChunkDir) => {
    let chapterRange = `${chapterChunkDir[0]}-${chapterChunkDir[chapterChunkDir.length - 1]}`;

    console.log(`[${dataSource}][create-mobi][${bookID}][${chapterRange}] start job`)

    let coverImage = ''

    const markdownTemplate = [
      `% ${bookID} - ${chapterRange}`,
      ...chapterChunkDir.map((chapterDir, i) => {
        let chapterImageFiles = fs.readdirSync(`${downloadedBookDir}/${chapterDir}`).map(f => `./${chapterDir}/${f}`)
        if (i == 0) {
          coverImage = `${downloadedBookDir}/${chapterImageFiles[0]}`
        }
        return [
          `# Chapter ${chapterDir}`,
          '',
          ...chapterImageFiles.map((f, i) => `![${bookID} - Chapter ${f.split('/')[1]} - Page ${i + 1}](${downloadedBookDir}/${f})`)
        ].join("\n")
      }),
    ].join("\n\n")

    let bookChunkChaptersFileName = `${downloadedBookDir}/${bookID} - ${chapterRange}.md`;
    let bookChunkChaptersEpubFileName = `${downloadedBookDir}/${bookID} - ${chapterRange}.epub`;
    let bookChunkChaptersMobiFileName = `${downloadedBookDir}/${bookID} - ${chapterRange}.mobi`;
    fs.writeFileSync(bookChunkChaptersFileName, markdownTemplate);

    console.log(`[${dataSource}][create-mobi][${bookID}][${chapterRange}] create epub, md=${bookChunkChaptersFileName} output=${bookChunkChaptersEpubFileName}`)
    await new Promise((resolve, reject) => {
      require('child_process').exec(`pandoc "${bookChunkChaptersFileName}" --epub-cover-image=${coverImage} --toc -o "${bookChunkChaptersEpubFileName}"`, (err, stdout, stderr) => {
        if (err) {
          console.log(`[${dataSource}][create-mobi][${bookID}][${chapterRange}] pandoc error=${err}`)
          return reject(err)
        }
        console.log(`[${dataSource}][create-mobi][${bookID}][${chapterRange}] pandoc complete=${bookChunkChaptersEpubFileName}`)
        resolve()
      })
    })

    await new Promise((resolve, reject) => {
      let ps = require('child_process').spawn(`kindlegen`, [bookChunkChaptersEpubFileName, "-c2"])

      const log = data => console.log(data.toString())

      ps.stdout.on('data', log)
      ps.stderr.on('data', log)

      ps.on('close', function (code) {
        if (code !== 0) {
          console.log(`[${dataSource}][create-mobi][${bookID}][${chapterRange}] kindlegen error=${err}`)
          return reject()
        }
        console.log(`[${dataSource}][create-mobi][${bookID}][${chapterRange}] kindlegen complete=${bookChunkChaptersMobiFileName}`)
        resolve()
      });
    })

  })

  console.log(`[${dataSource}][create-mobi][${bookID}] completed, total files=${chapterChunksDir.length}`)
}

main()