#!/usr/bin/env node

const prompts = require('prompts');
const MainController = require('./controller/main-controller')
const _ = require('lodash')
const fs = require('fs');

const pLimit = require('p-limit');

const limit = pLimit(1);
const limitM = pLimit(1);

const startCLI = async () => {

  let response = await prompts([
    {
      type: 'select',
      name: 'dataSource',
      message: `Choose dataSource?`,
      choices: [
        { title: 'Komikgue.com', value: 'komikgue' },
        { title: 'PecintaKomik.net', value: 'pecintakomik', selected: true },
      ],
    },
    {
      type: 'multiselect',
      name: 'action',
      message: 'Choose Action',
      choices: [
        { title: 'Update Library', value: 'updateLibrary' },
        { title: 'Fetch Book Detail', value: 'fetchBookDetail' },
        { title: 'List Book', value: 'listBook', selected: true },
        { title: 'List Book Chapter', value: 'listBookChapter', selected: true },
        { title: 'Download All Book', value: 'downloadAllBook' },
        { title: 'Download Book', value: 'downloadBook' },
        { title: 'Download Book Chapter', value: 'downloadBookChapter' },
        { title: 'Create ZIP Archives', value: 'zipBook' },
        { title: 'Create MOBI Files', value: 'createMobi' },
      ],
    },
  ]);

  let controller = new MainController({ dataSource: response.dataSource })

  if (_.intersection(
    response.action,
    [
      'fetchBookDetail',
      'downloadBook',
      'downloadBookChapter',
      'zipBook',
      'listBook',
      'listBookChapter',
      'createMobi',
    ],
  ).length > 0) {
    if (response.action.includes('updateLibrary') || !fs.existsSync(`./libraries/${response.dataSource}/list.json`)) {
      delete response.action[response.action.indexOf('updateLibrary')]
      await controller.updateLibrary();
    }
  }

  if (_.intersection(
    response.action,
    [
      'fetchBookDetail',
      'downloadBook',
      'downloadBookChapter',
      'listBookChapter',
      'zipBook',
      'createMobi',
    ],
  ).length > 0) {
    response = {
      ...response,
      ...await prompts([
        {
          type: 'autocompleteMultiselect',
          limit: 10,
          name: 'bookIDs',
          message: 'Choose Book',
          choices: [
            ...require('../libraries/pecintakomik/list.json').map(book => ({
              title: book.title,
              value: book.id,
            }))
          ],
        }
      ])
    }
  }


  let actions = response.action instanceof Array ? response.action : [response.action]

  for (const action of actions) {
    switch (action) {
      case 'updateLibrary':
        await controller.updateLibrary();
        break;
      case 'fetchBookDetail':
        await controller.fetchBookDetail(response.bookIDs);
        break;
      case 'listBook':
        await controller.listBook();
        break;
      case 'listBookChapter':
        await Promise.all(response.bookIDs.map(bookID => limit(async () => {
          await controller.listBookChapter(bookID)
        })));
        break;
      case 'downloadAllBook':
        await controller.downloadAllBook();
        break;
      case 'downloadBook':
        await Promise.all(response.bookIDs.map(bookID => limit(async () => {
          await controller.downloadBook(bookID)
        })));
        break;
      case 'downloadBookChapter':
        await Promise.all(response.bookIDs.map(bookID => limit(async () => {
          const { chapterIDs } = await prompts([
            {
              type: 'autocompleteMultiselect',
              limit: 10,
              name: 'chapterIDs',
              message: 'Choose Book',
              choices: require(`../libraries/pecintakomik/book/${bookID}.json`).chapters.map(chapter => ({
                title: chapter.chapter_number,
                value: chapter.chapter_number,
              })),
            }
          ])
          await Promise.all(chapterIDs.map(chapterID=>limitM(async()=>{
            await controller.downloadBookChapter(bookID, chapterID);
          })))

        })));
        break;
      case 'zipBook':
        await Promise.all(response.bookIDs.map(bookID => limit(async () => {
          await controller.zipBook(bookID)
        })));
        break;
      case 'createMobi':
        await Promise.all(response.bookIDs.map(bookID => limit(async () => {
          await controller.createMobi(bookID)
        })));
        break;
    }
  }


}

startCLI()