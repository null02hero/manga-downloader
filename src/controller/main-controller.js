const _ = require('lodash')
const fs = require('fs')

const pLimit = require('p-limit');

const limit = pLimit(1);
const limitM = pLimit(1);

class MainController {

  constructor(props) {
    this.props = props

    this.driver = {
      downloadChapter: require(`../data-source/${props.dataSource}`).downloadChapter,
      fetchLibrary: require(`../data-source/${props.dataSource}`).fetchLibrary,
      fetchBook: require(`../data-source/${props.dataSource}`).fetchBook,
    }

  }
  async updateLibrary() {
    console.log(`[${this.props.dataSource}][update-library] start job`)
    await this.driver.fetchLibrary()
    console.log(`[${this.props.dataSource}][update-library] job completed`)

  }
  async fetchBookDetail(bookIDs) {
    console.log(`[${this.props.dataSource}][fetch-book] start job`)
    let libraryListFileName = `./libraries/${this.props.dataSource}/list.json`
    if (!fs.existsSync(libraryListFileName)) {
      console.log(`[${this.props.dataSource}][fetch-book] Library list not exist, please run UPDATE_LIBRARY first`)
      return
    }

    let books = loadJSON(`./libraries/${this.props.dataSource}/list.json`)

    if (bookIDs.length > 0) {
      books = books.filter(book => bookIDs.includes(book.id))
    }

    await Promise.all(books.map(book => limit(() => this.driver.fetchBook(book))))

    console.log(`[${this.props.dataSource}][fetch-book] job completed`)

  }
  async listBook() {
    console.log(`[${this.props.dataSource}][list-book] start job`)
    let libraryListFileName = `./libraries/${this.props.dataSource}/list.json`
    if (!fs.existsSync(libraryListFileName)) {
      console.log('Library list not exist, please run UPDATE_LIBRARY first')
      return
    }
    let books = loadJSON(libraryListFileName)

    books.forEach(book => {
      let bookDetail = loadJSON(`./libraries/${this.props.dataSource}/book/${book.id}.json`)
      console.log(`${book.id} | ${book.title} | ${bookDetail.chapters.length} Chapters`)
    })
    console.log(`[${this.props.dataSource}][list-book] job completed`)
  }

  async listBookChapter(bookID) {
    const { dataSource } = this.props
    console.log(`[${dataSource}][list-chapter][${bookID}] job started`)
    let bookDetailFileName = `./libraries/${dataSource}/book/${bookID}.json`
    if (!fs.existsSync(bookDetailFileName)) {
      console.log(`[${dataSource}][list-chapter][${bookID}] ERROR Book not found, please run UPDATE_LIBRARY or FETCH_BOOK first`)
      return
    }

    let book = loadJSON(bookDetailFileName)

    book.chapters.reverse().forEach(chapter => console.log(`${bookID} | ${chapter.chapter_number} | ${chapter.release_date}`))

    console.log(`[${dataSource}][list-chapter][${bookID}] completed`)

  }

  async downloadAllBook() {
    console.log(`[${this.props.dataSource}][download-all-manga] start job`)
    let libraryListFileName = `./libraries/${this.props.dataSource}/list.json`
    if (!fs.existsSync(libraryListFileName)) {
      console.log('Library list not exist, please run UPDATE_LIBRARY first')
      return
    }

    let books = loadJSON(libraryListFileName)

    await Promise.all(books.map(book => limit(() => downloadManga(downloadChapter)(dataSource, book.id))))
    console.log(`[${dataSource}][download-all-manga] job completed`)
  }

  async downloadBook(bookID) {
    const { dataSource } = this.props
    console.log(`[${dataSource}][download-manga][${bookID}] job started`)
    let bookDetailFileName = `./libraries/${dataSource}/book/${bookID}.json`

    if (!fs.existsSync(bookDetailFileName)) {
      console.log(`[${dataSource}][download-manga][${bookID}] ERROR Book not found, please run UPDATE_LIBRARY or FETCH_BOOK first`)
      return
    }

    let book = loadJSON(bookDetailFileName)

    let totalChapters = book.chapters.length
    let completed = 0
    await Promise.all(book.chapters.reverse().map(chapter => limitM(async () => {
      let chapterNumber = chapter.chapter_number

      console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] start job [${completed + 1}/${totalChapters}]`)
      await this.driver.downloadChapter(bookID, chapterNumber)
      ++completed
      console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] completed [${completed}/${totalChapters}]`)
      return
    })))

    console.log(`[${dataSource}][download-manga][${bookID}] completed`)
    return
  }

  async downloadBookChapter(bookID, chapterNumber) {
    const { dataSource } = this.props
    console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] start job`)
    await this.driver.downloadChapter(bookID, chapterNumber)
    console.log(`[${dataSource}][download-manga-chapter][${bookID}][${chapterNumber}] completed`)

  }

  async zipBook() {

  }

  async createMobi() {

  }
}

const loadJSON = filename => {
  return JSON.parse(fs.readFileSync(filename).toString())
}

module.exports = MainController