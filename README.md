# Manga downloader

Script for download manga from various website

# Quick start

## Clone Project

```bash
git clone https://gitlab.com/null02hero/manga-downloader.git 
cd manga-downloader
npm install # or yarn install
```

## Supported data source

- komikgue
- pecintakomik

## Update Library

```bash
DATA_SOURCE=komikgue UPDATE_LIBRARY=1 FETCH_BOOK=1 LIST_BOOK=1 node src/index.js
```

## List manga

```bash
DATA_SOURCE=komikgue LIST_BOOK=1 node src/index.js
```

## Download specific manga

```bash
DATA_SOURCE=komikgue DOWNLOAD_BOOK=1 BOOK_ID=onepunch-man node src/index.js
```

or multiple book

```bash
DATA_SOURCE=komikgue DOWNLOAD_BOOK=1 BOOK_ID=onepunch-man,death-note node src/index.js
```

## List manga chapter

```bash
DATA_SOURCE=komikgue LIST_BOOK_CHAPTER=1 BOOK_ID=onepunch-man node src/index.js
```

## Download specific manga chapter

```bash
DATA_SOURCE=komikgue DOWNLOAD_BOOK_CHAPTER=1 BOOK_ID=onepunch-man CHAPTER_NUMBER=711 node src/index.js
```

## Create zip

```bash
DATA_SOURCE=komikgue ZIP_BOOK=1 BOOK_ID=onepunch-man node src/index.js
```

## Create e-book

```bash
DATA_SOURCE=komikgue CREATE_MOBI=1 BOOK_ID=onepunch-man node src/index.js
```